<?php
/*************************************************************************
 * Generated via "php artisan localization:missing" at 2017/03/18 20:27:44
 *************************************************************************/

return array(
    //============================== New strings to translate ==============================//
    'error'   => 'Error',
    'info'    => 'Info',
    'success' => 'Success',
    'warning' => 'Warning',
);