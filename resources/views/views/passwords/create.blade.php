@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Create Password') }}</div>

                <div class="panel-body">
                	<form action="{{ action('PasswordsController@store') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<div class="form-group  {{ ($errors->has('password')) ? 'has-error' : '' }}">
							<label for="password" class="col-sm-3 control-label">{{ trans('mongos.Password') }}</label>

							<div class="col-sm-8">
								<input type="text" name="password" class="form-control" value="{{ old('password') }}" placeholder="{{ trans('mongos.Password') }}">
								<p class="help-block">{{ ($errors->has('password') ?  $errors->first('password') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('level')) ? 'has-error' : '' }}">
							<label for="level" class="col-sm-3 control-label">{{ trans('mongos.Level') }}</label>

							<div class="col-sm-8">
								<input type="text" name="level" class="form-control" value="{{ old('level') }}" placeholder="{{ trans('mongos.Level') }}">
								<p class="help-block">{{ ($errors->has('level') ?  $errors->first('level') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('scraper_group_id')) ? 'has-error' : '' }}">
							<label for="scraper_group_id" class="col-sm-3 control-label">{{ trans('mongos.Scraper Group') }}</label>

							<div class="col-sm-8">
								<select name="scraper_group_id" class="form-control">
									@foreach($scrapers as $scraper)
									<option value="{{ $scraper->id }}" {{ old('scraper_group_id') == $scraper->id ? 'selected' : '' }}>{{ $scraper->name }}</option>
									@endforeach
								</select>
								<p class="help-block">{{ ($errors->has('scraper_group_id') ?  $errors->first('scraper_group_id') : '') }}</p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-8">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-check"></i> {{ trans('mongos.Save') }}
								</button>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
