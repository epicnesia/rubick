@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Passwords List') }}</div>

                <div class="panel-body">
                	<div class="row" style="margin-bottom: 15px;">
                		<div class="col-sm-6">
                			<a href="{{ action('PasswordsController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('mongos.Add Password') }}</a>
                    	</div>
                    	<div class="col-sm-6">
	                    	<select id="group" name="group" class="form-control">
	                    		@foreach($groups as $group)
	                    		<option value="{{ action('PasswordsController@index', ['gr'=>$group->id]) }}" {{ $group->id == app('request')->input('gr') ? 'selected' : '' }}>{{ $group->name }}</option>
	                    		@endforeach
	                    	</select>
	                    </div>
                    </div>
                    @if(count($passwords) > 0)
                    <div class="table-responsive">
                    	<table class="table table-bordered table-hover">
                    		<thead>
                    			<th>{{ trans('mongos.Password') }}</th>
                    			<th>{{ trans('mongos.Level') }}</th>
                    			<th>{{ trans('mongos.Group') }}</th>
                    			<th>{{ trans('mongos.Created At') }}</th>
                    			<th>{{ trans('mongos.Options') }}</th>
                    		</thead>
                    		<tbody class="sortable">
                    			@foreach($passwords as $password)
                    			<tr id="{{ $password->id }}">
                    				<td>{{ $password->password }}</td>
                    				<td class='level'>{{ $password->level }}</td>
                    				<td>{{ $password->scraper_group->name }}</td>
                    				<td>{{ $password->created_at->toFormattedDateString() }}</td>
                    				<td>
                    					<form action="{{ action('PasswordsController@destroy', [$password->id]) }}" method="POST" onsubmit="return confirm('{{ trans('mongos.Are you sure to delete this item?') }}');">
                    						{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE" />
											<a href="{{ action('PasswordsController@edit', [$password->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('mongos.Edit') }}</a>
                    						<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> {{ trans('mongos.Delete') }}</button>
                    					</form>
                    				</td>
                    			</tr>
                    			@endforeach
                    		</tbody>
                    	</table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
