@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Accounts List') }}</div>

                <div class="panel-body">
                	<div style="margin-bottom: 15px;">
                		<a href="{{ action('AccountsController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('mongos.Add Account') }}</a>
                    </div>
                    @if(count($accounts) > 0)
                    <div class="table-responsive">
                    	<table class="table table-bordered table-hover">
                    		<thead>
                    			<th>{{ trans('mongos.Username') }}</th>
                    			<th>{{ trans('mongos.Instagram ID') }}</th>
                    			<th>{{ trans('mongos.Group') }}</th>
                    			<th>{{ trans('mongos.Password') }}</th>
                    			<th>{{ trans('mongos.Created At') }}</th>
                    			<th>{{ trans('mongos.Options') }}</th>
                    		</thead>
                    		<tbody>
                    			@foreach($accounts as $account)
                    			<tr>
                    				<td>{{ $account->username }}</td>
                    				<td>{{ $account->instagram_id }}</td>
                    				<td>{{ $account->scraper_group->name }}</td>
                    				<td>{{ $account->password }}</td>
                    				<td>{{ $account->created_at->toFormattedDateString() }}</td>
                    				<td>
                    					<form action="{{ action('AccountsController@destroy', [$account->id]) }}" method="POST" onsubmit="return confirm('{{ trans('mongos.Are you sure to delete this item?') }}');">
                    						{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE" />
											<a href="{{ action('AccountsController@edit', [$account->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('mongos.Edit') }}</a>
                    						<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> {{ trans('mongos.Delete') }}</button>
                    					</form>
                    				</td>
                    			</tr>
                    			@endforeach
                    		</tbody>
                    	</table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
