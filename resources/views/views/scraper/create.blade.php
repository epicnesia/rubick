@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Create Scraper Group') }}</div>

                <div class="panel-body">
                	<form action="{{ action('ScraperController@store') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="is_brute_force" value="1" />
						<input type="hidden" name="is_scrap" value="1" />
						<div class="form-group  {{ ($errors->has('name')) ? 'has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">{{ trans('mongos.Name') }}</label>

							<div class="col-sm-8">
								<input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="{{ trans('mongos.Name') }}">
								<p class="help-block">{{ ($errors->has('name') ?  $errors->first('name') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('hashtags')) ? 'has-error' : '' }}">
							<label for="hashtags" class="col-sm-3 control-label">{{ trans('mongos.Hashtags') }}</label>

							<div class="col-sm-8">
								<textarea name="hashtags" class="form-control" rows="5"  placeholder="{{ trans('mongos.Hashtags') }}">{{ old('hashtags') }}</textarea>
								<p class="help-block">{{ ($errors->has('hashtags') ?  $errors->first('hashtags') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('usernames')) ? 'has-error' : '' }}">
							<label for="usernames" class="col-sm-3 control-label">{{ trans('mongos.Usernames') }}</label>

							<div class="col-sm-8">
								<textarea name="usernames" class="form-control" rows="5"  placeholder="{{ trans('mongos.Usernames') }}">{{ old('usernames') }}</textarea>
								<p class="help-block">{{ ($errors->has('usernames') ?  $errors->first('usernames') : '') }}</p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-8">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-check"></i> {{ trans('mongos.Save') }}
								</button>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
