@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Scraper Groups List') }}</div>

                <div class="panel-body">
                	<div style="margin-bottom: 15px;">
                		<a href="{{ action('ScraperController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('mongos.Add Scraper Group') }}</a>
                    </div>
                    @if(count($scrapers) > 0)
                    <div class="table-responsive">
                    	<table class="table table-bordered table-hover">
                    		<thead>
                    			<th>{{ trans('mongos.Name') }}</th>
                    			<th>{{ trans('mongos.Hashtags') }}</th>
                    			<th>{{ trans('mongos.Usernames') }}</th>
                    			<th>{{ trans('mongos.Created At') }}</th>
                    			<th colspan="3">{{ trans('mongos.Options') }}</th>
                    		</thead>
                    		<tbody>
                    			@foreach($scrapers as $scraper)
                    			<tr>
                    				<td>{{ $scraper->name }}</td>
                    				<td>{{ $scraper->hashtags }}</td>
                    				<td>{{ $scraper->usernames }}</td>
                    				<td>{{ $scraper->created_at->toFormattedDateString() }}</td>
                    				<td>
                    					<a href="{{ action('ScraperController@updateBruteForce', [$scraper->id]) }}" class="btn {{ $scraper->is_brute_force == 1 ? 'btn-danger' : 'btn-success' }}"> 
                    						{{ $scraper->is_brute_force == 1 ? trans('mongos.Pause Brute Force') : trans('mongos.Start Brute Force') }}
                    					</a>
                    				</td>
                    				<td>
                    					<a href="{{ action('ScraperController@updateScraper', [$scraper->id]) }}" class="btn {{ $scraper->is_scrap == 1 ? 'btn-danger' : 'btn-success' }}"> 
                    						{{ $scraper->is_scrap == 1 ? trans('mongos.Pause Scrap') : trans('mongos.Start Scrap') }}
                    					</a>
                    				</td>
                    				<td>
                    					<form action="{{ action('ScraperController@destroy', [$scraper->id]) }}" method="POST" onsubmit="return confirm('{{ trans('mongos.Are you sure to delete this item?') }}');">
                    						{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE" />
											<a href="{{ action('ScraperController@edit', [$scraper->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('mongos.Edit') }}</a>
                    						<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> {{ trans('mongos.Delete') }}</button>
                    					</form>
                    				</td>
                    			</tr>
                    			@endforeach
                    		</tbody>
                    	</table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
