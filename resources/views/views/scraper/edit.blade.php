@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Edit Scraper Group') }}</div>

                <div class="panel-body">
                	<form action="{{ action('ScraperController@update', [$scraper->id]) }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="PUT" />
						<div class="form-group  {{ ($errors->has('name')) ? 'has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">{{ trans('mongos.Name') }}</label>

							<div class="col-sm-8">
								<input type="text" name="name" class="form-control" value="{{ old('name', $scraper->name) }}" placeholder="{{ trans('mongos.Name') }}">
								<p class="help-block">{{ ($errors->has('name') ?  $errors->first('name') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('hashtags')) ? 'has-error' : '' }}">
							<label for="hashtags" class="col-sm-3 control-label">{{ trans('mongos.Hashtags') }}</label>

							<div class="col-sm-8">
								<textarea name="hashtags" class="form-control" rows="5"  placeholder="{{ trans('mongos.Hashtags') }}">{{ old('hashtags', $scraper->hashtags) }}</textarea>
								<p class="help-block">{{ ($errors->has('hashtags') ?  $errors->first('hashtags') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('usernames')) ? 'has-error' : '' }}">
							<label for="usernames" class="col-sm-3 control-label">{{ trans('mongos.Usernames') }}</label>

							<div class="col-sm-8">
								<textarea name="usernames" class="form-control" rows="5"  placeholder="{{ trans('mongos.Usernames') }}">{{ old('usernames', $scraper->usernames) }}</textarea>
								<p class="help-block">{{ ($errors->has('usernames') ?  $errors->first('usernames') : '') }}</p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-8">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-check"></i> {{ trans('mongos.Update') }}
								</button>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
