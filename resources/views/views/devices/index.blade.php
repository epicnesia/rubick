@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Devices List') }}</div>

                <div class="panel-body">
                	<div style="margin-bottom: 15px;">
                		<a href="{{ action('DevicesController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('mongos.Add Device') }}</a>
                    </div>
                    @if(count($devices) > 0)
                    <div class="table-responsive">
                    	<table class="table table-bordered table-hover">
                    		<thead>
                    			<th>{{ trans('mongos.Name') }}</th>
                    			<th>{{ trans('mongos.Common ID') }}</th>
                    			<th>{{ trans('mongos.Mac Address') }}</th>
                    			<th>{{ trans('mongos.IP Address') }}</th>
                    			<th>{{ trans('mongos.Group') }}</th>
                    			<th>{{ trans('mongos.City') }}</th>
                    			<th>{{ trans('mongos.Country') }}</th>
                    			<th>{{ trans('mongos.Created At') }}</th>
                    			<th>{{ trans('mongos.Options') }}</th>
                    		</thead>
                    		<tbody>
                    			@foreach($devices as $device)
                    			<tr>
                    				<td>{{ $device->name }}</td>
                    				<td>{{ $device->common_id }}</td>
                    				<td>{{ $device->mac_address }}</td>
                    				<td>{{ $device->ip_address }}</td>
                    				<td>{{ $device->scraper_group->name }}</td>
                    				<td>{{ $device->city }}</td>
                    				<td>{{ $device->country }}</td>
                    				<td>{{ $device->created_at->toFormattedDateString() }}</td>
                    				<td>
                    					<form action="{{ action('DevicesController@destroy', [$device->id]) }}" method="POST" onsubmit="return confirm('{{ trans('mongos.Are you sure to delete this item?') }}');">
                    						{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE" />
											<a href="{{ action('DevicesController@edit', [$device->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('mongos.Edit') }}</a>
                    						<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> {{ trans('mongos.Delete') }}</button>
                    					</form>
                    				</td>
                    			</tr>
                    			@endforeach
                    		</tbody>
                    	</table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
