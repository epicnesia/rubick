@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('mongos.Create Device') }}</div>

                <div class="panel-body">
                	<form action="{{ action('DevicesController@store') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<div class="form-group  {{ ($errors->has('name')) ? 'has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">{{ trans('mongos.Name') }}</label>

							<div class="col-sm-8">
								<input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="{{ trans('mongos.Name') }}">
								<p class="help-block">{{ ($errors->has('name') ?  $errors->first('name') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('common_id')) ? 'has-error' : '' }}">
							<label for="common_id" class="col-sm-3 control-label">{{ trans('mongos.Common ID') }}</label>

							<div class="col-sm-8">
								<input type="text" name="common_id" class="form-control" value="{{ old('common_id') }}" placeholder="{{ trans('mongos.Common ID') }}">
								<p class="help-block">{{ ($errors->has('common_id') ?  $errors->first('common_id') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('mac_address')) ? 'has-error' : '' }}">
							<label for="mac_address" class="col-sm-3 control-label">{{ trans('mongos.Mac Address') }}</label>

							<div class="col-sm-8">
								<input type="text" name="mac_address" class="form-control" value="{{ old('mac_address') }}" placeholder="{{ trans('mongos.Mac Address') }}">
								<p class="help-block">{{ ($errors->has('mac_address') ?  $errors->first('mac_address') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('city')) ? 'has-error' : '' }}">
							<label for="city" class="col-sm-3 control-label">{{ trans('mongos.City') }}</label>

							<div class="col-sm-8">
								<input type="text" name="city" class="form-control" value="{{ old('city') }}" placeholder="{{ trans('mongos.City') }}">
								<p class="help-block">{{ ($errors->has('city') ?  $errors->first('city') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('country')) ? 'has-error' : '' }}">
							<label for="country" class="col-sm-3 control-label">{{ trans('mongos.Country') }}</label>

							<div class="col-sm-8">
								<input type="text" name="country" class="form-control" value="{{ old('country') }}" placeholder="{{ trans('mongos.Country') }}">
								<p class="help-block">{{ ($errors->has('country') ?  $errors->first('country') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('scraper_group_id')) ? 'has-error' : '' }}">
							<label for="scraper_group_id" class="col-sm-3 control-label">{{ trans('mongos.Scraper Group') }}</label>

							<div class="col-sm-8">
								<select name="scraper_group_id" class="form-control">
									@foreach($scrapers as $scraper)
									<option value="{{ $scraper->id }}" {{ old('scraper_group_id') == $scraper->id ? 'selected' : '' }}>{{ $scraper->name }}</option>
									@endforeach
								</select>
								<p class="help-block">{{ ($errors->has('scraper_group_id') ?  $errors->first('scraper_group_id') : '') }}</p>
							</div>
						</div>
						<div class="form-group  {{ ($errors->has('ip_address')) ? 'has-error' : '' }}">
							<label for="ip_address" class="col-sm-3 control-label">{{ trans('mongos.IP Address') }}</label>

							<div class="col-sm-8">
								<input type="text" name="ip_address" class="form-control" value="{{ old('ip_address') }}" placeholder="{{ trans('mongos.IP Address') }}">
								<p class="help-block">{{ ($errors->has('ip_address') ?  $errors->first('ip_address') : '') }}</p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-8">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-check"></i> {{ trans('mongos.Save') }}
								</button>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
