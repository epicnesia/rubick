<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }}</title>

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/jquery-ui.structure.css') }}" rel="stylesheet">
		<link href="{{ asset('css/jquery-ui.theme.min.css') }}" rel="stylesheet">

		<!-- Scripts -->
		<script src="https://use.fontawesome.com/299728a74e.js"></script>
		<script>
			window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
			]) !!};
		</script>
	</head>
	<body>
		<div id="app">
			
			@section('navbar')
			@include('layouts.navbar')
			@show
			
			<div class="container-fluid">
				<!-- Notifications -->
				@section('notifications')
				@include('layouts.notifications')
				@show
				<!-- ./ notifications -->
				@yield('content')

			</div><!-- /.container -->

			@yield('footer')
		</div>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
		<script type="text/javascript">
		
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
		
			$('#group').change(function(e){
				window.location = $(this).val();
			});
		
			$('.sortable').sortable({
				stop: function(e, ui) {
					var lData = $.map($(this).find('tr'), function(el) {
									return el.id;
								});
					$.ajax({
						   type: "POST",
						   data: {level_data:lData},
						   url: "{{ action('PasswordsController@updateLevel') }}"
						});
						
					$.each(lData, function(i,v) {
						$('tr#' + v + ' > td.level').html(i+1);
					});
				}
			});
			$( "#sortable" ).disableSelection();
		</script>
	</body>
</html>
