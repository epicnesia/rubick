@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users List</div>

                <div class="panel-body">
                    <div class="table-responsive">
                    	<table class="table table-bordered table-hover">
                    		<thead>
                    			<th>Name</th>
                    			<th>Email</th>
                    			<th>Created At</th>
                    		</thead>
                    		<tbody>
                    			@foreach($users as $user)
                    			<tr>
                    				<td>{{ $user->name }}</td>
                    				<td>{{ $user->email }}</td>
                    				<td>{{ $user->created_at->toFormattedDateString() }}</td>
                    			</tr>
                    			@endforeach
                    		</tbody>
                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
