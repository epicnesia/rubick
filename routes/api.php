<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'mongoApi'], function () {

	// Accounts
	Route::post('accounts_has_password', 'Api\ApiAccountsController@ahp');
	Route::resource('accounts', 'Api\ApiAccountsController');

	// Devices
	Route::put('devices/ping', 'Api\ApiDevicesController@ping');
	Route::post('devices/history', 'Api\ApiDevicesController@storeHistory');
	Route::resource('devices', 'Api\ApiDevicesController');

	// Passwords
	Route::resource('passwords', 'Api\ApiPasswordsController');
	
});
