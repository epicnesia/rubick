<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\DevicesMapper::class,
        \App\Console\Commands\DevicesChecker::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('devices:match')->everyMinute()->withoutOverlapping()->runInBackground()
											   ->appendOutputTo('/tmp/device_mapper.log');
        $schedule->command('devices:check')->everyFiveMinutes()->withoutOverlapping()->runInBackground()
											   ->appendOutputTo('/tmp/device_checker.log');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
