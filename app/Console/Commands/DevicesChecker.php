<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Accounts;
use App\Devices;
use App\DevicesMapping;
use App\ScraperGroup;

class DevicesChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'devices:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the last ping and update devices activity';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
        	
			$now = Carbon::now();
			
			Devices::chunk(100, function($devices) use($now){
				
				foreach($devices as $device){
					
					$status = 'active';
					
					if($now->diffInDays($device->last_ping) > 3){
						$device->is_active = 0;
						$device->save();
						
						$status = 'inactive';
					}
					
					$this->line('Device '.$device->name.' is '.$status);
				}
				
			});
			 
			$this->line(trans('mongos.All devices processed'));
				
    	} catch (\Exception $e) {
			Log::error($e);
			$this->error($e->getLine() . ' ' . $e->getMessage());
        }
    }
}
