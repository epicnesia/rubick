<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Accounts;
use App\Devices;
use App\DevicesMapping;
use App\ScraperGroup;

class DevicesMapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'devices:match';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get account and match it with device.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
        	
			$pDevices = DevicesMapping::select('devices_id')->get()->toArray();
			
			Devices::whereNotIn('id', $pDevices)->chunk(100, function($devices){
				
				foreach($devices as $device){
					$exists	= DevicesMapping::where('devices_id', '=', $device->id)->first();
					if(!$exists){
						Accounts::orderBy('created_at', 'asc')->chunk(100, function($accounts) use ($device){
							
							foreach ($accounts as $account) {
								
								$cAccount = DevicesMapping::where('accounts_id', '=', $account->id)->first();
								if(!$cAccount){
									
									DevicesMapping::create([
															'devices_id'=>$device->id,
															'accounts_id'=>$account->id
															]);
									
									$this->line($device->name .' => '. $account->id);
									break;
									
								}
								
							}
							
						});
					}
				}
				
			});
			 
			$this->line(trans('mongos.All devices processed'));
				
    	} catch (\Exception $e) {
			Log::error($e);
			$this->error($e->getLine() . ' ' . $e->getMessage());
        }
    }
}
