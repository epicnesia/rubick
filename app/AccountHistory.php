<?php

namespace App;

use Moloquent;

class AccountHistory extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'account_history';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'accounts_id', 'history',
    ];
}