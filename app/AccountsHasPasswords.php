<?php

namespace App;

use Moloquent;

class AccountsHasPasswords extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'accounts_has_passwords';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'accounts_id', 'passwords_id', 'level', 'status', 'devices_id',
    ];
}