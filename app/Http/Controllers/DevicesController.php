<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Devices;
use App\ScraperGroup;

class DevicesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
			
			$devices	= Devices::paginate(30);
			
			return view('views.devices.index')->with('devices', $devices);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
        	
			$scraperGroup	= ScraperGroup::all();
			
			return view('views.devices.create')->with('scrapers', $scraperGroup);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			$this->validate($request, [
				'name'				=> 'required',
				'common_id'			=> 'required',
				'mac_address'		=> 'required',
				'city'				=> 'required',
				'country'			=> 'required',
				'scraper_group_id'	=> 'required',
				'ip_address'		=> 'required',
            ]);
			
			Devices::create($request->all());
			
			return redirect()->action('DevicesController@index')->with('success', trans('mongos.Device created successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
        	
			$device	= Devices::find($id);
			$scraperGroup	= ScraperGroup::all();
			
			return view('views.devices.edit')->with('device', $device)->with('scrapers', $scraperGroup);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
			
			$this->validate($request, [
				'name'				=> 'required',
				'common_id'			=> 'required',
				'mac_address'		=> 'required',
				'city'				=> 'required',
				'country'			=> 'required',
				'scraper_group_id'	=> 'required',
				'ip_address'		=> 'required',
            ]);
			
			$device						= Devices::find($id);
			$device->name				= $request->name;
			$device->common_id			= $request->common_id;
			$device->mac_address		= $request->mac_address;
			$device->city				= $request->city;
			$device->country			= $request->country;
			$device->scraper_group_id	= $request->scraper_group_id;
			$device->ip_address			= $request->ip_address;
			$device->save();
			
			return redirect()->action('DevicesController@index')->with('success', trans('mongos.Device updated successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
			
			$device = Devices::find($id);
			$device->delete();
			
			return redirect()->action('DevicesController@index')->with('success', trans('mongos.Device deleted successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }
}
