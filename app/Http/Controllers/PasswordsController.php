<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Passwords;
use App\ScraperGroup;

class PasswordsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
        	
			$groups			= ScraperGroup::orderBy('name')->get();
			$firstGroup		= ScraperGroup::orderBy('name')->first();
			
			$passwords		= Passwords::where('scraper_group_id', '=', $firstGroup->id)->orderBy('level')->get();
			
			if($request->has('gr'))
				$passwords		= Passwords::where('scraper_group_id', '=', $request->gr)->orderBy('level')->get();
			
			return view('views.passwords.index')->with('passwords', $passwords)
												->with('groups', $groups);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
        	
			$scraperGroup	= ScraperGroup::all();
			
			return view('views.passwords.create')->with('scrapers', $scraperGroup);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			$this->validate($request, [
				'password'			=> 'required',
				'level'				=> 'required',
				'scraper_group_id'	=> 'required',
            ]);
			
			$check_password1	= Passwords::where('scraper_group_id', '=', $request->scraper_group_id)
											->where('password', '=', $request->password)->first();
			$check_password2	= Passwords::where('scraper_group_id', '=', $request->scraper_group_id)
											->where('level', '=', $request->level)->first();
			if(!is_null($check_password1) || !is_null($check_password2)) throw new Exception(trans('mongos.Password exists'));
			
			Passwords::create($request->all());
			
			return redirect()->action('PasswordsController@index')->with('success', trans('mongos.Password created successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
        	
			$password		= Passwords::find($id);
			$scraperGroup	= ScraperGroup::all();
			
			return view('views.passwords.edit')->with('password', $password)->with('scrapers', $scraperGroup);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
			
			$this->validate($request, [
				'password'			=> 'required',
				'level'				=> 'required',
				'scraper_group_id'	=> 'required',
            ]);
			
			$password					= Passwords::find($id);
			
			if($password->password != $request->password){
				$check_password1	= Passwords::where('scraper_group_id', '=', $request->scraper_group_id)
												->where('password', '=', $request->password)->first();
				if(!is_null($check_password1)) throw new Exception(trans('mongos.Password exists'));
			}
			
			if($password->level != $request->level){
				$check_password2	= Passwords::where('scraper_group_id', '=', $request->scraper_group_id)
												->where('level', '=', $request->level)->first();
				if(!is_null($check_password2)) throw new Exception(trans('mongos.Password exists'));
			}
			
			$password->password			= $request->password;
			$password->level			= $request->level;
			$password->scraper_group_id	= $request->scraper_group_id;
			$password->save();
			
			return redirect()->action('PasswordsController@index')->with('success', trans('mongos.Password updated successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
			
			$password = Passwords::find($id);
			$password->delete();
			
			return redirect()->action('PasswordsController@index')->with('success', trans('mongos.Password deleted successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }
	
	public function updateLevel(Request $request)
    {
        try {
			
			$this->validate($request, [
				'level_data' => 'required|array',
            ]);
			
			foreach ($request->level_data as $key => $id) {
				
				$password = Passwords::find($id);
				$password->level = $key + 1;
				$password->save();
				
			}
			
			return 'success';
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return trans('mongos.This Action is Unauthorized');
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return $e->getMessage();

            return $e->getMessage();
        }
    }
}
