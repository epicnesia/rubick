<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Accounts;
use App\AccountHistory;
use App\AccountsHasPasswords;
use App\DevicesMapping;
use App\ScraperGroup;

class ApiAccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			
			$map	= DevicesMapping::where('devices_id', '=', $request->devices_id)->first();
			
			return response()->json(['result' => 'success', 'data'=>$map->accounts]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			$this->validate($request, [
				'username'			=> 'required',
				'instagram_id'		=> 'required|numeric',
				'scraper_group_id'	=> 'required'
            ]);
			
			Accounts::create($request->all());
			
			return response()->json(['result' => 'success', 'data'=>['message'=> trans('mongos.Account created successfully')]]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
			
			$this->validate($request, [
				'username'			=> 'required',
				'instagram_id'		=> 'required|numeric',
				'scraper_group_id'	=> 'required'
            ]);
			
			$account					= Accounts::find($id);
			$account->username			= $request->username;
			$account->instagram_id		= $request->instagram_id;
			$account->scraper_group_id	= $request->scraper_group_id;
			$account->password			= $request->password;
			$account->save();
			
			return response()->json(['result' => 'success', 'data'=>['message'=> trans('mongos.Account created successfully')]]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function ahp(Request $request)
	{
		try {
			
			$this->validate($request, [
				'accounts_id'		=> 'required',
				'passwords_id'		=> 'required',
				'level'				=> 'required',
				'status'			=> 'required',
				'devices_id'	=> 'required'
            ]);
			
			AccountsHasPasswords::create($request->all());
			AccountHistory::create([
				'accounts_id' 	=> $request->accounts_id,
				'history'		=> 'Accounts set',
			]);
			
			return response()->json(['result' => 'success', 'data'=>['message'=> trans('mongos.Data inserted successfully')]]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
	}
}
