<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Devices;
use App\DeviceHistories;
use App\ScraperGroup;

class ApiDevicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			$this->validate($request, [
				'name'				=> 'required',
				'common_id'			=> 'required',
				'mac_address'		=> 'required',
				'city'				=> 'required',
				'country'			=> 'required',
				'scraper_group_id'	=> 'required',
				'ip_address'		=> 'required',
            ]);
			
			Devices::create($request->all());
			
			return response()->json(['result' => 'success', 'data'=>['message'=> trans('mongos.Device created successfully')]]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
    }
	
	public function storeHistory(Request $request)
    {
        try {
			
			$this->validate($request, [
				'devices_id'	=> 'required',
				'history'		=> 'required',
            ]);
			
			DeviceHistories::create($request->all());
			
			return response()->json(['result' => 'success', 'data'=>['message'=> trans('mongos.Device history inserted')]]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
			
			$this->validate($request, [
				'name'				=> 'required',
				'common_id'			=> 'required',
				'mac_address'		=> 'required',
				'city'				=> 'required',
				'country'			=> 'required',
				'scraper_group_id'	=> 'required',
				'ip_address'		=> 'required',
            ]);
			
			$device						= Devices::find($id);
			$device->name				= $request->name;
			$device->common_id			= $request->common_id;
			$device->mac_address		= $request->mac_address;
			$device->city				= $request->city;
			$device->country			= $request->country;
			$device->scraper_group_id	= $request->scraper_group_id;
			$device->ip_address			= $request->ip_address;
			$device->save();
			
			return response()->json(['result' => 'success', 'data'=>['message'=> trans('mongos.Device updated successfully')]]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
     * Ping receiver
	 * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function ping(Request $request)
	{
		try {
			
			$this->validate($request, [
				'devices_id'			=> 'required',
            ]);
			
			$device						= Devices::find($request->devices_id);
			$device->last_ping			= Carbon::now()->toDateTimeString();
			$device->is_active			= 1;
			$device->save();
			
			return response()->json(['result' => 'success', 'data'=>['message'=> trans('mongos.Device updated successfully')]]);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> trans('mongos.This Action is Unauthorized')]]);
        } catch (\Exception $e) {
            Log::error($e);
			return response()->json(['result' => 'error', 'data'=>['message'=> $e->getMessage()]]);
        }
	}
}
