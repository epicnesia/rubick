<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\ScraperGroup;

class ScraperController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
			
			$scrapers	= ScraperGroup::paginate(30);
			
			return view('views.scraper.index')->with('scrapers', $scrapers);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
			
			return view('views.scraper.create');
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			$this->validate($request, [
				'name'			=> 'required',
				'hashtags'		=> 'required',
				'usernames'		=> 'required',
				'is_brute_force'=> 'required',
				'is_scrap'		=> 'required',
            ]);
			
			ScraperGroup::create($request->all());
			
			return redirect()->action('ScraperController@index')->with('success', trans('mongos.Scraper group created successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
        	
			$scraper	= ScraperGroup::find($id);
			
			return view('views.scraper.edit')->with('scraper', $scraper);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
			
			$this->validate($request, [
				'name'		=> 'required',
				'hashtags'	=> 'required',
				'usernames'	=> 'required',
            ]);

			$scraper 			= ScraperGroup::find($id);
			$scraper->name		= $request->name;
			$scraper->hashtags	= $request->hashtags;
			$scraper->usernames	= $request->usernames;
			$scraper->save();
			
			return redirect()->action('ScraperController@index')->with('success', trans('mongos.Scraper group updated successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

	public function updateBruteForce($id)
	{
		try {

			$scraper 					= ScraperGroup::find($id);
			$scraper->is_brute_force	= $scraper->is_brute_force == 1 ? 0 : 1;
			$scraper->save();
			
			return redirect()->action('ScraperController@index');
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
	}

	public function updateScraper($id)
	{
		try {

			$scraper 			= ScraperGroup::find($id);
			$scraper->is_scrap	= $scraper->is_scrap == 1 ? 0 : 1;
			$scraper->save();
			
			return redirect()->action('ScraperController@index');
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

			$scraper 			= ScraperGroup::find($id);
			$scraper->delete();
			
			return redirect()->action('ScraperController@index')->with('success', trans('mongos.Scraper group deleted successfully'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('mongos.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
