<?php

namespace App\Http\Middleware;

use Closure;

class MongoApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if($request->header('key') == '123456')
        	return $next($request);
		
		return response()->json(['result' => 'error', 'data'=>['message'=> 'Unauthenticated.']]);
    }
}
