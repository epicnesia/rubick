<?php

namespace App;

use Moloquent;

class Devices extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'devices';
	
	protected $dates = ['last_ping'];
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'common_id', 'mac_address', 'status', 'city', 'country', 'scraper_group_id', 'last_ping', 'is_active', 'ip_address',
    ];
	
	/*
	 * Scraper Group
	 */
	public function scraper_group()
    {
        return $this->belongsTo('App\ScraperGroup');
    }
}