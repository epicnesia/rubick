<?php

namespace App;

use Moloquent;

class DeviceHistories extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'device_histories';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'devices_id', 'history',
    ];
}
