<?php

namespace App;

use Moloquent;

class Passwords extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'passwords';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password', 'level', 'scraper_group_id',
    ];
	
	/*
	 * Scraper Group
	 */
	public function scraper_group()
    {
        return $this->belongsTo('App\ScraperGroup');
    }
}