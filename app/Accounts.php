<?php

namespace App;

use Moloquent;

class Accounts extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'accounts';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'instagram_id', 'scraper_group_id', 'password', 'status', 'taken'
    ];
	
	/*
	 * Scraper Group
	 */
	public function scraper_group()
    {
        return $this->belongsTo('App\ScraperGroup');
    }
}