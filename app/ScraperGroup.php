<?php

namespace App;

use Moloquent;

class ScraperGroup extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'scraper_group';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'hashtags', 'usernames', 'scraped',
    ];
	
	/*
	 * Accounts
	 */
	public function accounts()
    {
        return $this->hasMany('App\Accounts');
    }
	
	/*
	 * Devices
	 */
	public function devices()
    {
        return $this->hasMany('App\Devices');
    }
}
