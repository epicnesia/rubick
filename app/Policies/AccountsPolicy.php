<?php

namespace App\Policies;

use App\User;
use App\Accounts;
use Illuminate\Auth\Access\HandlesAuthorization;

class AccountsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the accounts.
     *
     * @param  \App\User  $user
     * @param  \App\Accounts  $accounts
     * @return mixed
     */
    public function view(User $user, Accounts $accounts)
    {
        //
    }

    /**
     * Determine whether the user can create accounts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the accounts.
     *
     * @param  \App\User  $user
     * @param  \App\Accounts  $accounts
     * @return mixed
     */
    public function update(User $user, Accounts $accounts)
    {
        //
    }

    /**
     * Determine whether the user can delete the accounts.
     *
     * @param  \App\User  $user
     * @param  \App\Accounts  $accounts
     * @return mixed
     */
    public function delete(User $user, Accounts $accounts)
    {
        //
    }
}
