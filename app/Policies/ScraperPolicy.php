<?php

namespace App\Policies;

use App\User;
use App\ScraperGroup;
use Illuminate\Auth\Access\HandlesAuthorization;

class ScraperPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the scraperGroup.
     *
     * @param  \App\User  $user
     * @param  \App\ScraperGroup  $scraperGroup
     * @return mixed
     */
    public function view(User $user, ScraperGroup $scraperGroup)
    {
        //
    }

    /**
     * Determine whether the user can create scraperGroups.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the scraperGroup.
     *
     * @param  \App\User  $user
     * @param  \App\ScraperGroup  $scraperGroup
     * @return mixed
     */
    public function update(User $user, ScraperGroup $scraperGroup)
    {
        //
    }

    /**
     * Determine whether the user can delete the scraperGroup.
     *
     * @param  \App\User  $user
     * @param  \App\ScraperGroup  $scraperGroup
     * @return mixed
     */
    public function delete(User $user, ScraperGroup $scraperGroup)
    {
        //
    }
}
