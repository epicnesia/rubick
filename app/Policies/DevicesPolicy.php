<?php

namespace App\Policies;

use App\User;
use App\Devices;
use Illuminate\Auth\Access\HandlesAuthorization;

class DevicesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the devices.
     *
     * @param  \App\User  $user
     * @param  \App\Devices  $devices
     * @return mixed
     */
    public function view(User $user, Devices $devices)
    {
        //
    }

    /**
     * Determine whether the user can create devices.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the devices.
     *
     * @param  \App\User  $user
     * @param  \App\Devices  $devices
     * @return mixed
     */
    public function update(User $user, Devices $devices)
    {
        //
    }

    /**
     * Determine whether the user can delete the devices.
     *
     * @param  \App\User  $user
     * @param  \App\Devices  $devices
     * @return mixed
     */
    public function delete(User $user, Devices $devices)
    {
        //
    }
}
