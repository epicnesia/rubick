<?php

namespace App;

use Moloquent;

class DevicesMapping extends Moloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'devices_mapping';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'devices_id', 'accounts_id'
    ];
	
	/*
	 * Devices
	 */
	public function devices()
    {
        return $this->belongsTo('App\Devices');
    }
	
	/*
	 * Account
	 */
	public function accounts()
    {
        return $this->belongsTo('App\Accounts');
    }
}
