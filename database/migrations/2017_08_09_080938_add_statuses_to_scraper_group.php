<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusesToScraperGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scraper_group', function (Blueprint $table) {
            $table->string('is_brute_force');
            $table->string('is_scrap');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scraper_group', function (Blueprint $table) {
            $table->dropColumn('is_brute_force');
            $table->dropColumn('is_scrap');
        });
    }
}
